//
//  FCMobAnalytics.h
//  FCMobStat
//
//  Created by ZhouYou on 11.12.19.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FCMobUserModel.h"

NS_ASSUME_NONNULL_BEGIN


static NSString *const FCMobActionServer                =   @"http://59.110.216.92/";//军号服务器地址

static NSString *const FCMobActionType_showDetail       =   @"showDetail";//展开详情
static NSString *const FCMobActionType_show             =   @"show";//展现
static NSString *const FCMobActionType_closeDetail      =   @"closeDetail";//关闭详情
static NSString *const FCMobActionType_star             =   @"star";//点赞
static NSString *const FCMobActionType_comment          =   @"comment";//评论
static NSString *const FCMobActionType_collect          =   @"collect";//收藏
static NSString *const FCMobActionType_share            =   @"share";//分享
static NSString *const FCMobActionType_complain         =   @"complain";//投诉、踩、不感兴趣
static NSString *const FCMobActionType_play             =   @"play";//播放
static NSString *const FCMobActionType_stopPlay         =   @"stopPlay";//停止播放
static NSString *const FCMobActionType_playInFull       =   @"playInFull";//全屏播放
static NSString *const FCMobActionType_playOutFull      =   @"playOutFull";//退出全屏播放

//行为状态，0：默认，1：开始，2：结束
//当设置为1时，会自动保存当前时间，当设置为2时会自动根据行为uuid取出对应的1时的时间来计算duration
typedef enum : NSUInteger {
    FCMobAction_Normal,
    FCMobAction_Start,
    FCMobAction_End,
} FCMobAction_State;


@interface FCMobAnalytics : NSObject

//设备的uuid,与行为埋点保持一致
+ (NSString *)device_uuid;

/*
 以appID初始化；userID可识别用户，若设置为nil则无法分析用户id的行为。
 自动统计start行为
 */
+ (void)startWithAppID:(NSString *)appID userID:(int)userID;

/*
 若已登录，需设置用户相关信息
 */
+ (void)setUserModel:(FCMobUserModel *)model;

/*
 设置userID
 若start时没有userID或者用户切换，可在此单独重新设置userID
 */
+ (void)setUserID:(NSString *)userID API_DEPRECATED("此方法不再提供使用,已于1.0.5及以后的版本中废弃",macos(1.0,1.0), ios(1.0,1.0), watchos(1.0,1.0));

/*
 设置行为上报定时发送间隔，默认90s,最低60s,最高300s
 */
+ (void)setReportTimerinterval:(NSTimeInterval)interavl;

/*
 若能获取到appdelete terminate的通知，建议调用一次此方法，统计terminate行为
 */
+ (void)willTerminate;

/*
 需要埋点的行为
 action 不能为空，行为类型，参考顶部定义
 itemID 可以为空，若行为关联内容，则需填写
 itemType :1资讯，2图集，3 banner，4视频，5服务，6直播，7电视，8广播，9广告，10音频
 */

+ (void)action:(NSString *)action itemID:(nullable NSString *)itemID API_DEPRECATED("此方法不再提供使用,已于1.0.5及以后的版本中废弃，请使用action:itemID:itemType:",macos(1.0,1.0), ios(1.0,1.0), watchos(1.0,1.0));

+ (void)action:(NSString *)action itemID:(nullable NSString *)itemID itemType:(nullable NSString *)itemType;

/*
需要埋点的行为
action 不能为空，行为类型，参考顶部定义
itemID 可以为空，若行为关联内容，则需填写
uuid 某些行为需要自定义uuid 比如，展开详情，关闭详情，需在此传入同一个uuid
itemType :1资讯，2图集，3 banner，4视频，5服务，6直播，7电视，8广播，9广告，10音频
state,当设置为1时，会自动保存当前时间，当设置为2时会自动根据行为uuid取出对应的1时的时间来计算duration
*/
+ (void)action:(NSString *)action uuid:(nullable NSString *)uuid itemID:(nullable NSString *)itemID itemType:(nullable NSString *)itemType actionState:(FCMobAction_State)state;

+ (void)action:(NSString *)action uuid:(nullable NSString *)uuid duration:(int)duration itemID:(nullable NSString *)itemID API_DEPRECATED("此方法不再提供使用,已于1.0.5及以后的版本中废弃",macos(1.0,1.0), ios(1.0,1.0), watchos(1.0,1.0));
@end

NS_ASSUME_NONNULL_END
