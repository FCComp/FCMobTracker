//
//  FCMobTracker.h
//  FCMobStat
//
//  Created by ZhouYou on 2020/7/22.
//  Copyright © 2020 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//static NSString *const FCMobTrackerServer                =   @"http://59.110.216.92/";//军号服务器地址

static NSString *const FCMobTrackerActionType_normal        =   @"normal";//初始状态普通事件,不具备任何意义
static NSString *const FCMobTrackerActionType_event         =   @"event";//采用eventID，eventLabel，eventAttribute，eventState的自定义事件
static NSString *const FCMobTrackerActionType_autoTracker   =   @"autoTracker";//采用自定抓取(无埋点)抓取的时间，可能有eventID，eventLabel，eventAttribute，eventState等数据值

static NSString *const FCMobTrackerEventID_ViewPage                 =   @"ViewPage";//初始定义的页面浏览eventID
static NSString *const FCMobTrackerEventID_ClickButton              =   @"ClickButton";//初始定义的UIButton点击事件
static NSString *const FCMobTrackerEventID_TableViewSelect          =   @"TableViewSelect";//初始定义的UITableView列表内容点击事件
static NSString *const FCMobTrackerEventID_CollectionViewSelect     =   @"CollectionViewSelect";//初始定义的UICollectionView列表内容点击事件
static NSString *const FCMobTrackerEventID_TapGesture               =   @"TapGesture";//初始定义的tap手势事件


typedef enum : NSUInteger {
    FCMobTrackerEventState_Default,
    FCMobTrackerEventState_Start,
    FCMobTrackerEventState_End,
} FCMobTrackerEventState;

@class FCMobUserModel;
@interface FCMobTracker : NSObject

/*
 后台服务器地址，样例：@"http://59.110.216.92/"，在startWithAppId前设置
 */
@property (nonatomic, copy) NSString *server_address;

/*
 项目分配的appKey，在startWithAppId前设置
 */
@property (nonatomic, copy) NSString *appKey;

/*
 项目分配的appKey，在startWithAppId前设置
 */
@property (nonatomic, copy) NSString *secretKey;

/*
设置用户自定义的用户识别id，在startWithAppId之前调用
设置一次UserId后，用户被永久标记。传入新的user_id将替换老的user_id。
传入nil或空字符串@""，可清空标记。
*/
@property (nonatomic, copy) NSString *user_code;

/*
 设置APP的版本号，默认取CFBundleShortVersionString中的版本号
 */
@property (nonatomic, copy) NSString *app_version;

/*
设置渠道Id，默认值为 "AppStore"
*/
@property (nonatomic, copy) NSString *app_channel;

/*
 设置应用进入后台再回到前台为同一次启动的最大间隔时间，有效值范围0～600s
 例如设置值30s，则应用进入后台后，30s内唤醒为同一次启动
 默认值 30s
 */
@property (nonatomic, assign) int sessionResumeInterval;

/*
 设置记录的上传时间间隔，有效范围10-600s，默认60s
 */
@property (nonatomic, assign) NSTimeInterval reportTimerInterval;
/*
是否打印日志，用于调试，默认No
*/
@property (nonatomic, assign) BOOL enableLog;

/*
 设备的唯一id，32位字符串，默认情况下会自动生成，也可自行设置。
 */
@property (nonatomic, copy) NSString *device_uuid;

/*
 app 启动来源，分三种：直接打开、push唤醒、app调起，默认为直接打开
 */
@property (nonatomic, copy) NSString *start_type;

#pragma mark - 使用enableGps注意
/*
 是否开启定位功能，定位采用高德sdk，默认为YES，需设置gaodeApiKey
 因高德sdk 2.8.0 开始增加了隐私合规信息:
    启用此功能前请确保APP已弹框显示隐私协议，且隐私协议内容包含『高德SDK隐私协议内容』，且用户已同意；
    否则请将enableGps设置为NO。
    详细信息请查看 "AMapLocationManager.h"  中 "Privacy 隐私合规"
 */
@property (nonatomic, assign) BOOL enableGps;

/*
 用于定位的高德apikey，需在startWithAppId前设置
 */
@property (nonatomic, copy) NSString *gaodeApiKey;

/*
 开启UIButton的自动抓取功能，默认NO，需在startWithAppId前设置
 */
@property (nonatomic, assign) BOOL enableUIButtonAutoTracker;

/*
 开启UITableView的didSelectRowAtIndexPath自动抓取功能，默认NO，需在startWithAppId前设置
 */
@property (nonatomic, assign) BOOL enableUITableViewAutoTracker;

/*
 开启UICollectionView的didSelectItemAtIndexPath自动抓取功能，默认NO，需在startWithAppId前设置
 */
@property (nonatomic, assign) BOOL enableUICollectionViewAutoTracker;

/*
 开启UIView的UITapGestureRecognizer自动抓取功能，默认NO，需在startWithAppId前设置
 */
@property (nonatomic, assign) BOOL enableUITapGestureRecognizerAutoTracker;


/*
 获取到一个统计对象实例
 */
+ (FCMobTracker *)defaultTracker;

/*
 采用appId初始化并启动
 */
+ (void)startWithAppId:(NSString *)appId;

/*
 设置用户信息，可在startWithAppId后调用
 */
+ (void)setUserModel:(FCMobUserModel *)model;

/*
 记录一次事件，eventID请预先创建，eventID为定义的时间ID,eventLabel为事件具体数据
 */
+ (void)logEvent:(NSString *)eventID eventLabel:(nullable NSString *)eventLabel;

/*
记录一次事件，eventID请预先创建，eventID为定义的时间ID;eventLabel为事件具体数据;duration为事件时长，秒为单位;attributes事件附加属性，value只支持NSString类型
*/
+ (void)logEvent:(NSString *)eventID eventLabel:(nullable NSString *)eventLabel duration:(NSInteger)duration attributes:(nullable NSDictionary *)attributes;

/*
记录一次事件开始，eventID请预先创建，eventID为定义的时间ID,eventLabel为事件具体数据
*/
+ (void)startEvent:(NSString *)eventID eventLabel:(nullable NSString *)eventLabel;

/*
记录一次事件结束，eventID请预先创建，eventID为定义的时间ID,eventLabel为事件具体数据
*/
+ (void)endEvent:(NSString *)eventID eventLabel:(nullable NSString *)eventLabel;

/*
记录一次事件结束，eventID请预先创建，eventID为定义的时间ID;eventLabel为事件具体数据;秒为单位;attributes事件附加属性，value只支持NSString类型
*/
+ (void)endEvent:(NSString *)eventID eventLabel:(nullable NSString *)eventLabel attributes:(nullable NSDictionary *)attributes;

/*
 记录开始浏览某个页面，name为页面名称
 */
+ (void)startViewPageWithName:(NSString *)name;

/*
记录结束浏览某个页面，name为页面名称
*/
+ (void)endViewPageWithName:(NSString *)name;

/*
 设备唯一id
 */
+ (NSString *)device_uuid;

/*
 供无埋点数据处理的方法，请勿调用
 */
+ (void)trackerStart:(NSString *)title data:(NSString *)data;

/*
 供无埋点数据处理的方法，请勿调用
 */
+ (void)trackerEnd:(NSString *)title data:(NSString *)data other:(NSDictionary *)otherInfo;

/*
 供无埋点数据处理的方法，请勿调用
 */
+ (void)trackerAction:(NSString *)action data:(NSString *)data time:(NSInteger)time other:(NSDictionary *)otherInfo;
@end

NS_ASSUME_NONNULL_END
