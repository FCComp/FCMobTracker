//
//  FCMobUserModel.h
//  FCMobStat
//
//  Created by ZhouYou on 12.12.19.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCMobUserModel : NSObject

@property (nonatomic, copy) NSString *user_code;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *real_name;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, assign) int age;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *head_pic;
@property (nonatomic, copy) NSString *origin;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *other;
@property (nonatomic, assign) int sex;

@end

NS_ASSUME_NONNULL_END
